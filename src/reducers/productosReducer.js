import {
  AGREGAR_PRODUCTO,
  AGREGAR_PRODUCTO_ERROR,
  AGREGAR_PRODUCTO_EXITO,
  DESCARGA_PRODUCTOS_COMENZAR,
  DESCARGA_PRODUCTOS_ERROR,
  DESCARGA_PRODUCTOS_EXITO,
  ELIMINAR_PRODUCTO_OBTENER,
} from "../types";

const initialState = {
  productos: [],
  error: null,
  loading: false,
  productoEliminar: null,
};

export default function (state = initialState, action) {
  switch (action.type) {
    case AGREGAR_PRODUCTO:
    case DESCARGA_PRODUCTOS_COMENZAR:
      return {
        ...state,
        loading: true,
      };
    case AGREGAR_PRODUCTO_EXITO:
      return {
        ...state,
        loading: false,
        productos: [...state.productos, action.payload],
      };
    case AGREGAR_PRODUCTO_ERROR:
    case DESCARGA_PRODUCTOS_ERROR:
      return {
        ...state,
        loading: false,
        error: action.payload,
      };
    case DESCARGA_PRODUCTOS_EXITO:
      return {
        ...state,
        loading: false,
        error: null,
        productos: action.payload,
      };
    case ELIMINAR_PRODUCTO_OBTENER:
      return {
        ...state,
        productoEliminar: action.payload,
      };
    default:
      return state;
  }
}
