import {
  AGREGAR_PRODUCTO,
  AGREGAR_PRODUCTO_ERROR,
  AGREGAR_PRODUCTO_EXITO,
  DESCARGA_PRODUCTOS_COMENZAR,
  DESCARGA_PRODUCTOS_ERROR,
  DESCARGA_PRODUCTOS_EXITO,
  ELIMINAR_PRODUCTO_OBTENER,
} from "../types";

import Swal from "sweetalert2";
import clienteAxios from "../config/axios";

//actions de crear nuevo producto
export function crearNuevoProductoAction(producto) {
  return async (dispatch) => {
    dispatch(agregarProducto());

    try {
      //insertar en la api
      await clienteAxios.post("/productos", producto);

      //si todo sale bien, actualiza el state
      dispatch(agregarProductoExito(producto));

      //mostrar mensaje de información
      Swal.fire("Correcto", "El producto se agregó correctamente", "success");
    } catch (error) {
      dispatch(agregarProductoError(true));

      //mostrar mensaje de error
      Swal.fire({
        icon: "error",
        title: "Hubo un error",
        text: "Hubo un error, intenta de nuevo",
      });
    }
  };
}

const agregarProducto = () => ({ type: AGREGAR_PRODUCTO });

const agregarProductoExito = (producto) => ({
  type: AGREGAR_PRODUCTO_EXITO,
  payload: producto,
});

const agregarProductoError = (estado) => ({
  type: AGREGAR_PRODUCTO_ERROR,
  payload: estado,
});

//actions de la descarga de productos para el listado
export function obtenerProductosAction() {
  return async (dispatch) => {
    dispatch(descarcarProductos());

    try {
      const respuesta = await clienteAxios.get("/productos");
      dispatch(descargaProductosExitosa(respuesta.data));
    } catch (error) {
      dispatch(descargaProductosError());
    }
  };
}

const descarcarProductos = () => ({
  type: DESCARGA_PRODUCTOS_COMENZAR,
  payload: true,
});

const descargaProductosExitosa = (productos) => ({
  type: DESCARGA_PRODUCTOS_EXITO,
  payload: productos,
});

const descargaProductosError = () => ({
  type: DESCARGA_PRODUCTOS_ERROR,
  payload: true,
});

//actions de eliminar producto
export function borrarProductoAction(id) {
  return async (dispatch) => {
    dispatch(obtenerProductoEliminar(id));
  };
}

const obtenerProductoEliminar = (id) => ({
  type: ELIMINAR_PRODUCTO_OBTENER,
  payload: id,
});
